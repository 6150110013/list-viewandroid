package com.programming.android.retrofitandroidpostexample;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface UserManagerInterface {

    public static String BASE_URL = "http://172.20.218.240/retrofit/";
    @FormUrlEncoded
    @POST("register.php")
    public Call<ResponseBody> registerUser(@Field("username") String userNameValue,
                                           @Field("password") String passwordValue,
                                           @Field("email") String emailValue);
}
