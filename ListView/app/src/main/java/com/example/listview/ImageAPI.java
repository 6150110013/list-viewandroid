package com.example.listview;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;



public interface ImageAPI {

    public static final String BASE_URL = "http://172.20.217.180/";
    @GET("/retrofit/list_images.php")
    Call<List<ImagesList>> getImages();
}
