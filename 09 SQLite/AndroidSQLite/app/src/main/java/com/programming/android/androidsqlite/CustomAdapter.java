package com.programming.android.androidsqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CustomAdapter extends BaseAdapter {


    private List<Contact> items;
    private Context context;
    private LayoutInflater inflater;

    public CustomAdapter(Context _context, List<Contact> _items){
        inflater = LayoutInflater.from(_context);
        this.items = _items;
        this.context = _context;
    }
    @Override
    public int getCount() {
        return items.size();
    }
    @Override
    public Object getItem(int position) {
        return position;
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Contact contact = items.get(position);

        View view = convertView;

        if(view == null)
            view = inflater.inflate(R.layout.contact_item, null);


        TextView name = (TextView) view.findViewById(R.id.tv_full_name);
        TextView phone = (TextView) view.findViewById(R.id.tv_phone_number);
        ImageView photo = (ImageView) view.findViewById(R.id.list_image);

        name.setText(contact.getName());
        phone.setText(contact.getPhoneNumber());
        photo.setImageBitmap(convertToBitmap(contact.getPhotograph()));

        return view;
    }
    private Bitmap convertToBitmap(byte[] b){
        return BitmapFactory.decodeByteArray(b, 0, b.length);
    }


}