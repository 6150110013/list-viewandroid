package com.example.mylayout;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
    }
    public void next2 (View view){
        Button click = findViewById(R.id.btn2);
        Intent intent2 = new Intent(MainActivity2.this,MainActivity3.class);
        startActivity(intent2);
    }
}