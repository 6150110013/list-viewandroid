package com.example.myfirstretrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MyInterface {
    public static final String BASE_URL = "https://www.w3schools.com/js/";
    @GET("customers_mysql.php")
    Call<List<Customer>> getCustomers();
    // Customer is POJO class to get the data from API,
    // use List<UserListResponse> in callback
    // because the data in our API is starting from JSONArray
}
